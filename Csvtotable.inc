<?php

function CSVtotable($tablenum)

{

// Csv file locations
$csv1 = variable_get('CSV_file1');
$csv2 = variable_get('CSV_file2');
$csv3 = variable_get('CSV_file3');
$csv4 = variable_get('CSV_file4');

switch($tablenum)
 {
 case 1:
 $file = $csv1;
 break;
 
 case 2:
 $file = $csv2;
 break;
 
 case 3:
 $file = $csv3;
 break;
 
 case 4:
 $file = $csv4;
 break;
 
}

//Read csv and display to html table

$blockcontent= "<table class='CSVtable'>\n\n";
if(file_exists($file))
{
	$f = fopen($file, "r");
	

	while (($line = fgetcsv($f)) !== false)
	{
			$blockcontent.= "<tr>";
			foreach ($line as $cell) 
			{
				$blockcontent.= "<td>" . htmlspecialchars($cell) . "</td>";
			}
			$blockcontent.= "</tr>\n";
	}
 fclose($f);
 $blockcontent.= "\n</table>";
 
}
else 
{
$blockcontent.= "File not Found :(";

}
return $blockcontent;
}


?>